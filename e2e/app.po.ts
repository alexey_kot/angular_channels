export class AngularChannelsPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('angular-channels-app h1')).getText();
  }
}
