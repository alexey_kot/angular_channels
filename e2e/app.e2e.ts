import { AngularChannelsPage } from './app.po';

describe('angular-channels App', function() {
  let page: AngularChannelsPage;

  beforeEach(() => {
    page = new AngularChannelsPage();
  })

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('angular-channels works!');
  });
});
