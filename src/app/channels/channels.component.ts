import { Component, OnInit } from '@angular/core';
import { AngularFire, FirebaseAuth, FirebaseListObservable, FirebaseObjectObservable, FirebaseAuthState } from 'angularfire2';
import { AuthService } from  '../auth/auth.service';

@Component({
  moduleId: module.id,
  selector: 'app-channels',
  templateUrl: 'channels.component.html',
  styleUrls: ['channels.component.css'],
  providers: [AuthService]
})
export class ChannelsComponent implements OnInit {

  public channels: FirebaseListObservable<any>;
  public item: FirebaseObjectObservable<any>;
  public currentUserUid: string;

  constructor(private af: AngularFire, private auth: FirebaseAuth, private authService: AuthService) {  
    
  }

  ngOnInit() {
    this.auth.subscribe(auth => this.currentUserUid = auth.uid);
    this.item = this.af.database.object('/users/' + this.currentUserUid + '/channels');
    this.channels = this.af.database.list('/users/' + this.currentUserUid + '/channels');
    }

   addChannel(newName: string) {
    let newChannel: FirebaseObjectObservable<any> = this.af.database.object('/channels/' + newName + '/members');
    newChannel.set({[this.currentUserUid]: 'true'});
  }
  updateChannel(newText: string) {
    // this.channels.update(key, { text: newText });
    // var jsonVar = {};
    // jsonVar[newText] = 'true';

    this.item.update({[newText]: 'true'});
    }
  deleteChannel(key: string) {    
    this.channels.remove(key); 
  }
  deleteEverything() {
    this.channels.remove();
  }

}
