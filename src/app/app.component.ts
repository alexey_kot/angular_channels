import { Component } from '@angular/core';
import { LoginComponent } from './auth/login.component';
import { RegisterComponent } from './auth/register.component';
import { ROUTER_DIRECTIVES } from '@angular/router';


@Component({
  moduleId: module.id,
  selector: 'angular-channels-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  directives: [LoginComponent, RegisterComponent, ROUTER_DIRECTIVES]
})
export class AppComponent {
  title = 'angular-channels works!';
}
