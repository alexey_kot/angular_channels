import { provideRouter, RouterConfig }  from '@angular/router';

import { LoginComponent } from './auth/login.component';
import { RegisterComponent } from './auth/register.component';
import { ChannelsComponent } from './channels/';


const routes: RouterConfig = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'channels',
    component: ChannelsComponent
  }
];

export const appRouterProviders = [
  provideRouter(routes)
];