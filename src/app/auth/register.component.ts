import { Component, OnInit } from '@angular/core';
import { NgForm }    from '@angular/forms';
import { AngularFire } from 'angularfire2';
import { AuthService } from  './auth.service';

@Component({
	moduleId: module.id,
	selector: 'app-auth',
	templateUrl: 'register.component.html',
	styleUrls: ['register.component.css'],
	providers: [AuthService]
})
export class RegisterComponent implements OnInit {

	public userEmail: string;
	public userPassword: string;
	public userName: string;

	public af: AngularFire;

	constructor(public authService: AuthService) {
		this.af = authService.af;
	}

	ngOnInit() {
	}

    register(userEmail: string, userPassword: string, userName: string) {
		this.authService.register(userEmail, userPassword, userName);
	}

	logout() {
		this.authService.logOut();
	}

}
