import { Component, OnInit } from '@angular/core';
import { NgForm }    from '@angular/forms';
import { AngularFire } from 'angularfire2';
import { AuthService } from  './auth.service';
import { Router } from '@angular/router';

@Component({
	moduleId: module.id,
	selector: 'app-auth',
	templateUrl: 'login.component.html',
	styleUrls: ['login.component.css'],
	providers: [AuthService]
})
export class LoginComponent implements OnInit {

	public userEmail: string;
	public userPassword: string;

	public af: AngularFire;

	constructor(private router: Router, public authService: AuthService) {
		this.af = authService.af;
	}

	ngOnInit() {
	}

	login(userEmail: string, userPassword: string) {
		this.authService.emailLogin(userEmail, userPassword);
	}

	logout() {
		this.authService.logOut();
	}

	goToChannels() {
		this.router.navigate(['/channels']);
	}

}
