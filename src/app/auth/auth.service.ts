import { Injectable } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods, FirebaseAuth, FirebaseAuthState } from 'angularfire2';

@Injectable()
export class AuthService {

    public authState: FirebaseAuthState = null;

  constructor(public af: AngularFire, public auth: FirebaseAuth) {
		this.af.auth.subscribe(auth => console.log("AF status " + auth.uid));
        this.auth.subscribe((state: FirebaseAuthState) => {
            this.authState = state;
        });
	}

	overrideLogin() {
		this.af.auth.login({
			provider: AuthProviders.Anonymous,
			method: AuthMethods.Anonymous,
		});    
	}

    // Email and password
    emailLogin(userEmail: string, userPassword: string) {
        this.af.auth.login(
        {
            email: userEmail,
            password: userPassword
        },
        {
            provider: AuthProviders.Password,
            method: AuthMethods.Password,
        }).then(_ => console.log('success login'))
        .catch(err => console.log(err));
    }

    logOut(){
    	this.af.auth.logout();
    }

    register(userEmail: string, userPassword: string, userName: string) {
        let uid: string;
        this.af.auth.createUser(
        {
            email: userEmail,
            password: userPassword,    
        }).then(_ => this.createUser(_, userEmail, userName))
        .catch(err => console.log(err));
        }

    createUser(user: FirebaseAuthState, userEmail: string, userName: string) {
        let newUser = this.af.database.object('users/' + user.uid);
        newUser.set({email: userEmail, name: userName});   
        console.log(user.uid);
    }

}
