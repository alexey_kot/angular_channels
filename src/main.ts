import { bootstrap } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { disableDeprecatedForms, provideForms } from '@angular/forms';
import { AppComponent, environment } from './app/';
import { FIREBASE_PROVIDERS, 
		  defaultFirebase, 
		  AngularFire, 
		  AuthMethods, 
		  AuthProviders, 
		  firebaseAuthConfig
		 } from 'angularfire2';
import { appRouterProviders } from './app/app.routes';

if (environment.production) {
  enableProdMode();
}

bootstrap(AppComponent, [
  appRouterProviders,
  FIREBASE_PROVIDERS,
  // Initialize Firebase app  
  defaultFirebase({
    apiKey: "AIzaSyA3_9Vzn3rK4KnCxTKK3DP6IY9KdWytn7c",
    authDomain: "mkdev-d9d98.firebaseapp.com",
    databaseURL: "https://mkdev-d9d98.firebaseio.com",
    storageBucket: "mkdev-d9d98.appspot.com",
  }),
  firebaseAuthConfig({
    provider: AuthProviders.Google,
    method: AuthMethods.Redirect
  }),
   disableDeprecatedForms(),
  provideForms()
])
.catch((err: any) => console.error(err));
